/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _mediator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./mediator */ \"./application/mediator.js\");\n\r\n\r\nObject(_mediator__WEBPACK_IMPORTED_MODULE_0__[\"default\"])();\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/mediator.js":
/*!*********************************!*\
  !*** ./application/mediator.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\r\n  Написать медиатор для группы студентов.\r\n\r\n  Профессор отвечает только на вопросы старосты.\r\n\r\n  У Студента есть имя и группа которой он пренадлежит.\r\n  Он может запросить старосту задать вопрос или получить ответ.\r\n\r\n  Староста может добавлять студентов в группу и передавать вопрос профессору.\r\n*/\r\n\r\nconst Mediator = () => {\r\n\r\n  class Professor {\r\n    constructor(name){\r\n      this.name = name;\r\n      this.type = 'professor';\r\n\r\n      console.log(`Professor ${this.name} arrived!`);\r\n    }\r\n    answerTheQuestion( question, monitor, student ){\r\n\r\n      if( monitor.type !== 'monitor'){\r\n        console.error(`It\\' not your bussines, ${student.name}`);\r\n      } else {\r\n        student.getAnswer( `Your question is too philosophical to be answered`, this);\r\n      }\r\n    }\r\n  }\r\n\r\n  class Student {\r\n    constructor(name){\r\n      this.name = name;\r\n      this.monitor = null;\r\n      this.type = 'student';\r\n\r\n      console.log(`Let's welcome a new student ${this.name}`);\r\n    }\r\n    getAnswer(message, from){      \r\n      console.log(`${from.name} tells ${this.name}: ${message}`);\r\n    }\r\n    tipTheMonitor( message, to, prof){ //передаём имя профессора, чтобы обратиться к конкретному профессору, если их несколько\r\n      if (this.monitor !== null){\r\n              \r\n        if (prof !== undefined){      //Небольшая логика, которая выводит в консоль текст в зависимости от адресата\r\n          console.log(`${this.name} tells ${prof.name}: ${message}`);\r\n        } else if ( to !== undefined ){\r\n          console.log(`${this.name} tells ${to.name}: ${message}`);\r\n        }\r\n\r\n        this.monitor.askProfessor( message, this, to, prof );\r\n\r\n      } else {\r\n        console.warn(`Please ask the monitor to add you to the group!`); //Если не добавлен в группу, то получает предупреждение\r\n      }\r\n    }\r\n  }\r\n\r\n  // Monitor == Староста\r\n  class Monitor extends Student{\r\n    constructor(name){\r\n      super(name);\r\n      this.name = name;\r\n      this.studentsInGroup = {};\r\n      this.type = 'monitor';\r\n\r\n      console.log(`${this.name} will be the monitor of the class`);\r\n    }\r\n    addToGroup(student){ //метод для добавления студента в группу\r\n      this.studentsInGroup[student.name] = student;\r\n      student.monitor = this;\r\n      console.log(`Student ${student.name} has been added to the group`);      \r\n      console.log(`List of students in the group:`, this.studentsInGroup);      \r\n    }\r\n    askProfessor(message, from, to, prof){\r\n\r\n      if ( to !== undefined && to !== this ){ //Если есть адресат, но он не ментор\r\n\r\n        to.answerTheQuestion( message, to, from );\r\n      } else if( to === this ){               //Если вопрос передаётся через ментора\r\n        prof.answerTheQuestion( message, to, from );\r\n      }else {                                 //Если вопрос адресован всем\r\n        for( let key in this.studentsInGroup ){\r\n          if( this.studentsInGroup[key] !== from ){\r\n            this.studentsInGroup[key].getAnswer(message, from);\r\n          }\r\n        }\r\n      }\r\n    }\r\n  }\r\n\r\n  const student1 = new Student('Petya');\r\n  const student2 = new Student('Vasya');\r\n  const student3 = new Student('Masha');\r\n  console.log(`***********************`);  \r\n  const prof1 = new Professor('Mr. Shwarz');\r\n  const prof2 = new Professor('Ms. Sasha Grey');\r\n  console.log(`***********************`);\r\n  const monitor = new Monitor('Gendalf');\r\n  console.log(`***********************`);  \r\n        monitor.addToGroup(student1);\r\n        monitor.addToGroup(student2);\r\n        monitor.addToGroup(student3);\r\n        student3.tipTheMonitor('hello!');\r\n        student1.getAnswer('hello!', prof2);\r\n        student1.tipTheMonitor('Wow! I want more lessons!');\r\n        student2.tipTheMonitor('What are you doing today?', prof2);        \r\n        student1.tipTheMonitor('Can I have additional lessons?', monitor, prof1);\r\n\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Mediator);\r\n\n\n//# sourceURL=webpack:///./application/mediator.js?");

/***/ })

/******/ });